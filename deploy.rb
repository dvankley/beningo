#!/usr/bin/ruby
require 'date'
require 'optparse'
require 'open3'
require 'pty'
require 'expect'
require 'thwait'
require 'net/ssh'
require 'io/console'

# If running in a new environment, ensure you've run the following commands successfully first:
# - yarn install
# - yarn build
# - sudo gem install net-ssh

# MODE = 'DEV'
MODE = 'PROD'

class DeployFrontend
  def main
    puts('You better be running this script from the root directory of the frontend project')

    # Parse CLI args
    ssh_host = nil
    ssh_username = nil

    optparser = OptionParser.new do |opts|
      opts.banner = 'Usage: frontend-deploy -h host -u username -p ssh-key-passphrase'

      opts.on('-h', '--host [INPUT_HOST]', String, 'ssh host') do |input_host|
        if input_host
          ssh_host = input_host.strip
        end
      end
      opts.on('-u', '--username [INPUT_USERNAME]', String, 'ssh host') do |input_username|
        if input_username
          ssh_username = input_username.strip
        end
      end
    end
    optparser.parse!
    if ssh_host.nil?
      raise 'Failed to parse ssh host'
    end
    if ssh_username.nil?
      raise 'Failed to parse ssh username'
    end

    if MODE == 'DEV'
      sudo_password = '?'
    else
      puts('Please input the sudo password for the server you\'re deploying to')
      sudo_password = STDIN.noecho(&:gets).strip
      if sudo_password.nil? || sudo_password.empty?
        raise 'Failed to parse password'
      end
    end

    if MODE == 'DEV'
      ssh_key_passphrase = '?!'
    else
      puts('Please input your ssh key passphrase')
      ssh_key_passphrase = STDIN.noecho(&:gets).strip
      # ssh_key_passphrase = gets.strip
      if ssh_key_passphrase.nil? || ssh_key_passphrase.empty?
        raise 'Failed to parse passphrase'
      end
    end

    puts('Building project to build/...')
    build_result, stdout = execute_and_stream('yarn build')
    unless build_result == 0
      puts("Project build failed with code #{build_result}")
    end

    puts('Zipping up distribution...')
    execute_and_stream('tar -czvf beningo.tar.gz build/*')

    puts('Copying artifact to server...')
    execute_and_stream("scp beningo.tar.gz #{ssh_username}@#{ssh_host}:~/")

    puts('Checking server home directory...')
    result = execute_ssh_command(ssh_host, ssh_username, sudo_password, ssh_key_passphrase, 'pwd')
    puts("server home: #{result}")

    puts('Clearing old artifact...')
    result = execute_ssh_command(ssh_host, ssh_username, sudo_password, ssh_key_passphrase, 'sudo rm -rf /var/www/html/beningo.djvk.net/*')

    puts('Deploying new artifact...')
    result = execute_ssh_command(ssh_host, ssh_username, sudo_password, ssh_key_passphrase, 'sudo tar --strip-components 1 -xzvf beningo.tar.gz -C /var/www/html/beningo.djvk.net/')

    puts('Updating file permissions...')
    result = execute_ssh_command(ssh_host, ssh_username, sudo_password, ssh_key_passphrase, 'sudo chgrp -R www-data /var/www/html/beningo.djvk.net/')
  end

  def execute_ssh_command(ssh_host, ssh_username, password, key_passphrase, cmd)
    result = ''
    Net::SSH.start(ssh_host, ssh_username, :password => password, :passphrase => key_passphrase) do |ssh|
      # Open a channel
      channel = ssh.open_channel do |channel, success|
        # Callback to receive data. It will read the
        # data and store it in result var or
        # send the password if it's required.
        channel.on_data do |channel, data|
          if data =~ /^\[sudo\] password for /
            # Send the password
            channel.send_data "#{password}\n"
          else
            # Store the data
            result += data.to_s
          end
        end
        # Request a pseudo TTY
        channel.request_pty
        # Execute the command
        channel.exec(cmd)
        # Wait for response
        channel.wait
      end
      # Wait for opened channel
      channel.wait
      result
    end
  end

  # @param [String] cmd
  # @return [Array<String>] [stdout, stderr]
  def execute_and_stream(cmd)
    Open3.popen2e(cmd) do |stdin, stdout_and_stderr, wait_thr|
      Thread.new do
        stdout_and_stderr.each {|l| puts l }
      end
      [wait_thr.value, stdout_and_stderr]
    end
  end
end

DeployFrontend.new.main
