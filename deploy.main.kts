#!/usr/bin/env kotlin

@file:DependsOn("com.hierynomus:sshj:0.31.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")
@file:DependsOn("org.bouncycastle:bcprov-jdk15on:1.68")
@file:DependsOn("org.bouncycastle:bcpkix-jdk15on:1.68")
@file:DependsOn("com.jcraft:jzlib:1.1.3")
@file:DependsOn("com.hierynomus:asn-one:0.5.0")
@file:DependsOn("net.i2p.crypto:eddsa:0.3.0")

import net.schmizz.sshj.SSHClient

println("test")

val ssh = SSHClient()
ssh.loadKnownHosts()
// ssh.connect()
